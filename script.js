const myButton = document.querySelector('.button');

myButton.addEventListener('click', function () {

    let email = document.querySelector('.email').value;
    let date = document.getElementById("birthdate").value;

    class Validator {
        static isEmail(email) {
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailRegex.test(email)) {
                console.error("Invalid email format");
                return false;
            }
            return true;
        }

        static isDate(date) {
            const dateRegex = /^\d{4}-\d{2}-\d{2}$/;
            if (!dateRegex.test(date)) {
                console.error("Invalid date format (YYYY-MM-DD)");
                return false;
            }
            const parsedDate = Date.parse(date);
            if (isNaN(parsedDate)) {
                console.error("Invalid date");
                return false;
            }
            return true;
        }
    }

    if (Validator.isEmail(email)) {
        alert(`${email} is a valid email`);
    } else {
        alert(`${email} is not a valid email`);
    }

    if (Validator.isDate(date)) {
        alert(`${date} is a valid date`);
    } else {
        alert(`${date} is not a valid date`);
    }
});